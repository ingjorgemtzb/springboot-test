package com.example.demo.mail.service.impl;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.mail.dto.EmailConfigurationDTO;
import com.example.demo.mail.service.EmailService;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGridAPI;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;

@Component("sendGridEmail")
public class SendGridEmailServiceImpl implements EmailService {

	private static final String MAIL_SEND = "mail/send";

	private static Logger logger = LoggerFactory.getLogger(SendGridEmailServiceImpl.class);

	private SendGridAPI sendGridAPI;

	@Autowired
	public void setSendGridAPI(SendGridAPI sendGridAPI) {
		this.sendGridAPI = sendGridAPI;
	}

	@Override
	public void sendText(String from, String to, String subject, String body) {
//		Response response = sendEmail(from, to, subject, new Content(TEXT_PLAIN, body));
//		logger.info("Status Code: {}, Body: {}, Headers: ", response.getStatusCode(), response.getBody(),
//				response.getHeaders());

		throw new UnsupportedOperationException("not yet implement");
	}

	@Override
	public void sendHTML(EmailConfigurationDTO emailConfigurationDTO) {

		Mail mail = makeEmailSendGrid(emailConfigurationDTO);

		Response response = sendEmail(mail);
		logger.info("Status Code: {}, Body: {}, Headers: ", response.getStatusCode(), response.getBody(),
				response.getHeaders());
	}

	private Mail makeEmailSendGrid(EmailConfigurationDTO emailConfigurationDTO) {
		Email emailFrom = new Email(emailConfigurationDTO.getSender());

		Mail mail = new Mail();
		emailConfigurationDTO.getRecipients().forEach(email -> {
			Email emailTo = new Email(email);
			Personalization personalization = new Personalization();
			emailConfigurationDTO.getPlaceholders().forEach((k, v) -> {
				personalization.addSubstitution(k, v);
			});
			personalization.addTo(emailTo);
			mail.addPersonalization(personalization);
		});

		// FIXME: It's unique?
		if (emailConfigurationDTO.isReply()) {
			emailConfigurationDTO.getReplyTo().forEach(email -> {
				mail.setReplyTo(new Email(email));
			});
		}

		mail.setSubject(emailConfigurationDTO.getSubject());
		mail.setFrom(emailFrom);
		mail.setTemplateId(emailConfigurationDTO.getTemplateId());
		return mail;
	}

	private Response sendEmail(Mail mail) {

		Request request = new Request();
		Response response = null;
		try {
			request.setMethod(Method.POST);
			request.setEndpoint(MAIL_SEND);
			request.setBody(mail.build());
			response = sendGridAPI.api(request);
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		}
		return response;
	}

}
