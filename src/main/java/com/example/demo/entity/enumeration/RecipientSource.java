package com.example.demo.entity.enumeration;

public enum RecipientSource {
	BODY_CLIENT, TABLE_RECIPIENT
}
