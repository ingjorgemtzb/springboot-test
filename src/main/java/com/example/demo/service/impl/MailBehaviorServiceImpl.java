package com.example.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.controller.dto.Proccess;
import com.example.demo.entity.MailBehavior;
import com.example.demo.entity.Seller;
import com.example.demo.repository.MailBehaviorRepository;
import com.example.demo.repository.SellerRepository;
import com.example.demo.service.MailBehaviorService;

@Service
public class MailBehaviorServiceImpl implements MailBehaviorService{

	@Autowired
	private MailBehaviorRepository mailBehaviorRepository;
	@Autowired
	private SellerRepository sellerRepository;
	
	@Override
	public List<MailBehavior> find(Proccess proccess) {
		Optional<Seller> seller= sellerRepository.findByProccess(proccess.getId());
		
		if(!seller.isPresent()) {
			throw new IllegalArgumentException("Not find seller by proccess");
		}
		
		
		return mailBehaviorRepository.findBySeller(seller.get());
	}

}
