package com.example.demo.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.demo.entity.MailBehavior;
import com.example.demo.entity.Seller;

public interface MailBehaviorRepository extends PagingAndSortingRepository<MailBehavior, Long>{
	public List<MailBehavior> findBySeller(Seller seller);
}
