package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.Recipient;
import com.example.demo.entity.RecipientGroup;
import com.example.demo.entity.Seller;

public interface RecipientService {
	public List<RecipientGroup>  find(Seller seller);
}
