package com.example.demo.runner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class ProfileInfoRunner implements ApplicationRunner{
	
	private static Logger logger = LoggerFactory.getLogger(ProfileInfoRunner.class);
	
	@Value("${spring.profiles.active}")
	private String profile;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		logger.info("prifile: " + profile);
		
	}


}
