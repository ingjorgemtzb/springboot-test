package com.example.demo.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.mongodb.core.mapping.Document;

@Entity
@Table(name = "tbl_seller")
@Document(collection = "sellers")
public class Seller implements Serializable{

	private static final long serialVersionUID = 3821375182459984984L;

	@Id
	@org.springframework.data.annotation.Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "str_name")
	private String name;
	
	@Column(name = "long_proccess")
	private Long proccess;
	
	@OneToMany(
	        mappedBy = "seller",
	        cascade = CascadeType.ALL,
	        orphanRemoval = true
	    )
	private List<RecipientGroup> recipientGroup = new ArrayList<>();
	
	@OneToMany(
	        mappedBy = "seller",
	        cascade = CascadeType.ALL,
	        orphanRemoval = true
	    )
	private List<MailBehavior> mailBehavior = new ArrayList<>();

	public Seller() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getProccess() {
		return proccess;
	}

	public void setProccess(Long proccess) {
		this.proccess = proccess;
	}

	public List<RecipientGroup> getRecipientGroup() {
		return recipientGroup;
	}

	public void setRecipientGroup(List<RecipientGroup> recipientGroup) {
		this.recipientGroup = recipientGroup;
	}

	public List<MailBehavior> getMailBehavior() {
		return mailBehavior;
	}

	public void setMailBehavior(List<MailBehavior> mailBehavior) {
		this.mailBehavior = mailBehavior;
	}
	
}
