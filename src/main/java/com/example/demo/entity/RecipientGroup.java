package com.example.demo.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.mongodb.core.mapping.Document;

import com.example.demo.entity.enumeration.RecipientGroupType;

@Entity
@Table(name = "tbl_recipient_group")
@Document(collection = "recipient_groups")
public class RecipientGroup implements Serializable {
	
	private static final long serialVersionUID = 6741632196883604939L;

	@Id
	@org.springframework.data.annotation.Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "str_name")
	private String name;
	
	@Column(name = "str_type")
	@Enumerated(EnumType.STRING)
	private RecipientGroupType type;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Seller seller;
	
	@ManyToMany(mappedBy = "recipientGroups")
	private Set<Recipient> recipients = new HashSet<>();

	public RecipientGroup() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public RecipientGroupType getType() {
		return type;
	}

	public void setType(RecipientGroupType type) {
		this.type = type;
	}

	public Seller getSeller() {
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

	public Set<Recipient> getRecipients() {
		return recipients;
	}

	public void setRecipients(Set<Recipient> recipients) {
		this.recipients = recipients;
	} 

}
