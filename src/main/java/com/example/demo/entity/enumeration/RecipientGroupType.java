package com.example.demo.entity.enumeration;

public enum RecipientGroupType {
	SENT_TO, REPLY_TO

}
