package com.example.demo.controller;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.MailBehavior;
import com.example.demo.entity.RecipientGroup;
import com.example.demo.entity.enumeration.RecipientGroupType;
import com.example.demo.mail.dto.EmailConfigurationDTO;
import com.example.demo.mail.service.EmailService;
import com.example.demo.service.MailBehaviorService;
import com.example.demo.service.RecipientService;

@Service
public class ProccessResolverImpl implements ProccessResolver {

	private Map<String, EmailService> emailService;

	private MailBehaviorService mailBehaviorService;
	private RecipientService recipientService;

	@Autowired
	public void setEmailService(Map<String, EmailService> emailService) {
		this.emailService = emailService;
	}

	@Autowired
	public void setMailBehaviorService(MailBehaviorService mailBehaviorService) {
		this.mailBehaviorService = mailBehaviorService;
	}

	@Autowired
	public void setRecipientService(RecipientService recipientService) {
		this.recipientService = recipientService;
	}

	@Override
	public void solve(InfoMailBody infoMail) {

		List<MailBehavior> mailBehaviors = mailBehaviorService.find(infoMail.getProccess());

		if (mailBehaviors == null || mailBehaviors.isEmpty()) {
			throw new IllegalArgumentException("not behavior configurate by proccess");
		}

		mailBehaviors.forEach(mailBehavior -> configureAndSent(mailBehavior, infoMail));

	}

	private void configureAndSent(MailBehavior mailBehavior, InfoMailBody infoMail) {
		EmailConfigurationDTO emailConfigurationDTO = makeEmailConfigurationDTO(mailBehavior, infoMail);

		switch (mailBehavior.getBuilderApproach()) {
		case LEGACYTEMPLATE:
			EmailService selectedEmailService = emailService.get("sendGridEmail");
			selectedEmailService.sendHTML(emailConfigurationDTO);
			break;

		default:
			break;
		}
	}

	private EmailConfigurationDTO makeEmailConfigurationDTO(MailBehavior mailBehavior, InfoMailBody infoMail) {
		EmailConfigurationDTO emailConfigurationDTO = new EmailConfigurationDTO();
		emailConfigurationDTO.setSender("ing.jorgemtzb@gmail.com");

		configureRecipients(mailBehavior, infoMail, emailConfigurationDTO);

		if (emailConfigurationDTO.getRecipients().isEmpty()) {
			throw new IllegalArgumentException("recipient list is empty");
		}

		emailConfigurationDTO.setPlaceholders(infoMail.getPlaceholders());
		emailConfigurationDTO.setReply(mailBehavior.isReply());
		
		emailConfigurationDTO.setSubject(mailBehavior.getSubject());
		emailConfigurationDTO.setTemplateId(mailBehavior.getTemplateId());
		return emailConfigurationDTO;
	}

	private void configureRecipients(MailBehavior mailBehavior, InfoMailBody infoMail,
			EmailConfigurationDTO emailConfigurationDTO) {
		switch (mailBehavior.getRecipientSource()) {
		case TABLE_RECIPIENT:
			List<RecipientGroup> recipientsGroup = recipientService.find(mailBehavior.getSeller());
			List<String> recipients = recipientsGroup.stream()
					.filter($ -> RecipientGroupType.SENT_TO.equals($.getType())).map(group -> group.getRecipients())
					.flatMap($ -> $.stream()).map($ -> $.getEmail()).collect(Collectors.toList());
			emailConfigurationDTO.setRecipients(recipients);

			if (mailBehavior.isReply()) {
				List<String> replyTo = recipientsGroup.stream()
						.filter($ -> RecipientGroupType.REPLY_TO.equals($.getType()))
						.map(group -> group.getRecipients()).flatMap($ -> $.stream()).map($ -> $.getEmail())
						.collect(Collectors.toList());
				emailConfigurationDTO.setReplyTo(replyTo);
			}

			break;

		case BODY_CLIENT:
			emailConfigurationDTO.setRecipients(Arrays.asList(infoMail.getThirdEmail()));
			break;
		default:
			emailConfigurationDTO.setRecipients(Collections.emptyList());
			break;
		}
	}

}
