package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.demo.entity.Seller;

public interface SellerRepository extends PagingAndSortingRepository<Seller, Long>{
	public Optional<Seller> findByProccess(Long id);
}
