package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.controller.wrapper.ResponseWrapper;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("api/mail")
public class ModuleController {
	
	private ProccessResolver proccessResolver;
	
	@Autowired
	public void setProccessResolver(ProccessResolver proccessResolver) {
		this.proccessResolver = proccessResolver;
	}



	@PostMapping()
	@ApiOperation(value = "Modulo de comunicaciones", notes = "unfinished")
	public ResponseWrapper<String> sendMail(@RequestBody InfoMailBody body) {
		
		body.validateModule().validateProccess().validateEmail();
		
		proccessResolver.solve(body);
		
		ResponseWrapper<String> response = new ResponseWrapper<String>();
		
		return response;
	}
	

}
