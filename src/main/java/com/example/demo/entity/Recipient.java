package com.example.demo.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.data.mongodb.core.mapping.Document;

@Entity
@Table(name = "tbl_recipient")
@Document(collection = "recipients")
public class Recipient implements Serializable {
	
	private static final long serialVersionUID = -6494507311447815681L;

	@Id
	@org.springframework.data.annotation.Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "str_name")
	private String name;
	@Column(name = "str_email")
	private String email;
	@Column(name = "str_position")
	private String position;
	
	@ManyToMany
	@JoinTable(
	  name = "tbl_recipient_group_recipient", 
	  joinColumns = @JoinColumn(name = "fk_recipient_id"), 
	  inverseJoinColumns = @JoinColumn(name = "fk_recipient_group_id"))
	private Set<RecipientGroup> recipientGroups = new HashSet<>();
	
	public Recipient() {
		super();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public Set<RecipientGroup> getRecipientGroups() {
		return recipientGroups;
	}
	public void setRecipientGroups(Set<RecipientGroup> recipientGroups) {
		this.recipientGroups = recipientGroups;
	}
	
}
