package com.example.demo.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.mongodb.core.mapping.Document;

@Entity
@Table(name = "tbl_request")
@Document(collection = "requests")
public class Request implements Serializable {
	
	private static final long serialVersionUID = -7602645992577589812L;

	@Id
	@org.springframework.data.annotation.Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "str_module_name")
	private String moduleName;
	
	@Column(name = "str_proccess_name")
	private String proccesName;
	
	@Column(name = "str_seller_name")
	private String sellerName;
	
	@Column(name = "tim_created_on")
	private LocalDateTime createdOn;
	
	public Request() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getProccesName() {
		return proccesName;
	}

	public void setProccesName(String proccesName) {
		this.proccesName = proccesName;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

}
