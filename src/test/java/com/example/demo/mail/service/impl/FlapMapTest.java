package com.example.demo.mail.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import com.example.demo.entity.Recipient;
import com.example.demo.entity.RecipientGroup;
import com.example.demo.entity.enumeration.RecipientGroupType;

public class FlapMapTest {

	@Test
	public void flapMap() {

		RecipientGroup g1 = new RecipientGroup();
		g1.setType(RecipientGroupType.SENT_TO);

		Set<Recipient> recipient = new HashSet<>();
		Recipient r1 = new Recipient();
		r1.setEmail("sent_to 1");

		Recipient r2 = new Recipient();
		r2.setEmail("sent_to 2");

		recipient.add(r1);
		recipient.add(r2);

		g1.setRecipients(recipient);

		RecipientGroup g2 = new RecipientGroup();
		g2.setType(RecipientGroupType.REPLY_TO);

		Set<Recipient> recipient2 = new HashSet<>();

		Recipient r3 = new Recipient();
		r3.setEmail("sent_to 3");

		Recipient r4 = new Recipient();
		r4.setEmail("sent_to 4");

		recipient2.add(r3);
		recipient2.add(r4);

		g2.setRecipients(recipient2);

		List<RecipientGroup> groups = new ArrayList<>();
		groups.add(g1);
		groups.add(g2);

		List<String> email = groups.stream().filter($ -> RecipientGroupType.SENT_TO.equals($.getType()))
				.map(group -> group.getRecipients()).flatMap($ -> $.stream()).map($ -> $.getEmail())
				.collect(Collectors.toList());

		assertEquals(2, email.size());

	}

}
