package com.example.demo.norelational.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("norelational")
public class NoSQLDataSourceConfig {

}
