package com.example.demo.service;

import java.util.List;

import com.example.demo.controller.dto.Proccess;
import com.example.demo.entity.MailBehavior;

public interface MailBehaviorService {
	public List<MailBehavior> find(Proccess proccess);
}
