package com.example.demo.controller.wrapper;

public class ResponseWrapper<T> {
	
	public StatusProccess status;
	public T entity;
	public String msg;
	
	public ResponseWrapper() {
		super();
	}

	public StatusProccess getStatus() {
		return status;
	}
	
	public void setStatus(StatusProccess status) {
		this.status = status;
	}
	public T getEntity() {
		return entity;
	}
	public void setEntity(T entity) {
		this.entity = entity;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
