package com.example.demo.controller.wrapper;

public class EmailValidator {

	private org.apache.commons.validator.routines.EmailValidator validator;

	public EmailValidator() {
		validator = org.apache.commons.validator.routines.EmailValidator.getInstance();
	}
	
	public boolean isValid(String email) {
		return validator.isValid(email);
	}

}
