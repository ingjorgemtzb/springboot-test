package com.example.demo.controller;

import java.util.Map;

import com.example.demo.controller.dto.Module;
import com.example.demo.controller.dto.Proccess;
import com.example.demo.controller.wrapper.EmailValidator;

import io.swagger.annotations.ApiParam;

public class InfoMailBody {

	@ApiParam(required = true)
	private Module module;
	@ApiParam(required = true)
	private Proccess proccess;
	private Map<String, String> placeholders;
	@ApiParam(required = true)
	private String thirdEmail;

	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

	public Proccess getProccess() {
		return proccess;
	}

	public void setProccess(Proccess proccess) {
		this.proccess = proccess;
	}

	public Map<String, String> getPlaceholders() {
		return placeholders;
	}

	public void setPlaceholders(Map<String, String> placeholders) {
		this.placeholders = placeholders;
	}

	public String getThirdEmail() {
		return thirdEmail;
	}

	public void setThirdEmail(String thirdEmail) {
		this.thirdEmail = thirdEmail;
	}

	public InfoMailBody validateModule() {
		try {
			getModule().getId().toString();
		} catch (NullPointerException e) {
			throw new IllegalArgumentException("module insconsistent");
		}
		return this;
	}

	public InfoMailBody validateProccess() {
		try {
			getProccess().getId().toString();
		} catch (NullPointerException e) {
			throw new IllegalArgumentException("proccess insconsistent");
		}
		return this;
	}

	public InfoMailBody validateEmail() {

		if (!new EmailValidator().isValid(getThirdEmail()))
			throw new IllegalArgumentException("email insconsistent");

		return this;
	}

}
