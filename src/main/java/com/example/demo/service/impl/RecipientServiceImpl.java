package com.example.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.example.demo.entity.RecipientGroup;
import com.example.demo.entity.Seller;
import com.example.demo.repository.RecipientGroupRepository;
import com.example.demo.service.RecipientService;

@Service
public class RecipientServiceImpl implements RecipientService {

	@Autowired
	public RecipientGroupRepository recipientGroupRepository;

	@Override
	@Cacheable(cacheNames = "recipientGroups", key = "#seller.id")
	public List<RecipientGroup> find(Seller seller) {
		return recipientGroupRepository.findBySeller(seller);
	}

}
