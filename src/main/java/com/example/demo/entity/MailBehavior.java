package com.example.demo.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.mongodb.core.mapping.Document;

import com.example.demo.entity.enumeration.BuilderApproach;
import com.example.demo.entity.enumeration.RecipientSource;

@Entity
@Table(name = "tbl_mail_behavior")
@Document(collection = "mailBehaviors")
public class MailBehavior implements Serializable{
	
	private static final long serialVersionUID = -26366416252307948L;

	@Id
	@org.springframework.data.annotation.Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "int_sequence")
	private Integer sequence;
	
	@Column(name = "str_template_id")
	private String templateId;
	
	@Column(name = "str_subject")
	private String subject;
	
	@Column(name = "str_recipient_source")
	@Enumerated(EnumType.STRING)
	private RecipientSource recipientSource;
	
	@Column(name = "str_builder_approach")
	@Enumerated(EnumType.STRING)
	private BuilderApproach builderApproach;
	
	@Column(name = "bool_reply")
	private boolean reply = false;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Seller seller;

	public MailBehavior() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public RecipientSource getRecipientSource() {
		return recipientSource;
	}

	public void setRecipientSource(RecipientSource recipientSource) {
		this.recipientSource = recipientSource;
	}

	public BuilderApproach getBuilderApproach() {
		return builderApproach;
	}

	public void setBuilderApproach(BuilderApproach builderApproach) {
		this.builderApproach = builderApproach;
	}

	public boolean isReply() {
		return reply;
	}

	public void setReply(boolean reply) {
		this.reply = reply;
	}

	public Seller getSeller() {
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
}
