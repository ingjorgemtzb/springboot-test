package com.example.demo.controller;

public interface ProccessResolver {
	
	public void solve(InfoMailBody infoMail);

}
