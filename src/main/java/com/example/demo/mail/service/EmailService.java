package com.example.demo.mail.service;

import com.example.demo.mail.dto.EmailConfigurationDTO;

public interface EmailService {
	void sendText(String from, String to, String subject, String body);
    void sendHTML(EmailConfigurationDTO emailConfigurationDTO);

}
