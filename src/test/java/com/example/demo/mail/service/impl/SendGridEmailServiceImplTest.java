package com.example.demo.mail.service.impl;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.mail.dto.EmailConfigurationDTO;
import com.example.demo.mail.service.EmailService;

@SpringBootTest
public class SendGridEmailServiceImplTest {
	
	@Autowired
	@Qualifier("sendGridEmail")
	private EmailService emailService;

	@Test
	@Disabled
	void sendTextTest() {
		emailService.sendHTML(new EmailConfigurationDTO());
	}
}
