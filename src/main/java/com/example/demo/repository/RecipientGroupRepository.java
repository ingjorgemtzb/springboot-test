package com.example.demo.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.demo.entity.RecipientGroup;
import com.example.demo.entity.Seller;


public interface RecipientGroupRepository extends PagingAndSortingRepository<RecipientGroup, Long>{
	public List<RecipientGroup> findBySeller(Seller seller);
}
